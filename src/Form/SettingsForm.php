<?php

namespace Drupal\retrict_site_access\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Retrict Site Access settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'retrict_site_access_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['retrict_site_access.settings'];
  }

  private function getRoles() {
    $user_role = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
    return array_map(function ($role) {
      return $role->label();
    }, $user_role);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['page_retrict_role'] = [
      '#type' => 'select',
      '#options' => $this->getRoles(),
      '#title' => $this->t('Roles'),
      '#description' => $this->t('Select role will be redirect.'),
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#size' => 3,
      '#default_value' => $this->config('retrict_site_access.settings')->get('page_retrict_role'),
    ];

    $form['exclude_url'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exclude URLs'),
      '#description' => 'Multiple exclude url seperated by comma. Ex: /admin/*, /user/login',
      '#default_value' => !empty(($this->config('retrict_site_access.settings')->get('exclude_url'))) ?
        $this->config('retrict_site_access.settings')->get('exclude_url') : '/user/login, /admin/*',
      '#required' => TRUE,
    ];

    $form['page_retrict_content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Retrict Page Content'),
      '#default_value' => $this->config('retrict_site_access.settings')->get('page_retrict_content.value'),
      '#format' => $this->config('retrict_site_access.settings')->get('page_retrict_content.format'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('retrict_site_access.settings')
      ->set('page_retrict_content', $form_state->getValue('page_retrict_content'))
      ->set('exclude_url', $form_state->getValue('exclude_url'))
      ->set('page_retrict_role', $form_state->getValue('page_retrict_role'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
